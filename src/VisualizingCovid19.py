#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 30 3 2021
#UpD:
#Descri: Data Visualizations

# Full Description:
#

# Imports:
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as image
import matplotlib.cbook as book


# See some versions
print("backends available: ", matplotlib.rcsetup.all_backends)
print("current backend: ", matplotlib.get_backend())

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

# DATA
# low memory option for mixed data types in column
norway = pd.read_csv('datasets/covid-19-tracker/data/norway.csv',
                     low_memory=False)
print("covid 19 Norway data head: ")
print(norway.head())

## features
# features definitions supplied codebook
# quick summary :
# Record number : numeric,
# endtime : string, date and time of record completion
# qweek : numeric, week of?
# i? : various, inventory questions for health
# d? : numeric, health diagnosis indicators
# weight : numeric, participant weight
# age : numeric, participant age
# region : numeric, coded UK region (may not apply to this data set)
# gender : numeric, coded gender (1 = male, 2 = female)
# household_size : numeric, number of people in household
# household_children : numeric, number of children in household
# employment_status : numeric, coded employment status (see codebook)
# WCR : numeric, qualitative responses to government handling of COVID-19 (see codebook)
# WCRV_4 : numeric, qualitative response to contracting covid (see codebook)
# CORE_B2_4,cantril_ladder, PHQ4_1-PHQ4_4: numeric, qualitative response to happiness
# m? : numeric, coded mask wearing behaviors
# v? : numeric, coded vaccine questions
# r? : numeric, coded perceived threat of COVID 19 questions
# ct? : numeric, coded contract tracing questions
# ox? : numeric, coded country of residence survey
# w? : numeric, coded wellness and mental health survey
# work? : numeric, coded occupation survey
# soc? : numeric, coded social behavior during the pandemic questions
# vac? : numeric, coded vaccine behavior questions
# av? : numeric, coded aversion to vaccine questions
# travel? : numeric, coded attitudes towards travel during COVID 19 pandemic


print_full(norway.describe())


