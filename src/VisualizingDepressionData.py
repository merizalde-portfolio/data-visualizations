#AUTH: Dr. Jaime Lopez-Merizalde
#Date: 29 3 2021
#UpD:
#Descri: Data Visualizations

# Full Description:
#

# Imports:
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as image
import matplotlib.cbook as book


# See some versions
print("backends available: ", matplotlib.rcsetup.all_backends)
print("current backend: ", matplotlib.get_backend())

# pre-functions
def print_full(x):
    pd.set_option('display.max_columns',10000)
    pd.set_option('display.max_rows', 10000)
    print(x)
    pd.reset_option('display.max_columns')
    pd.reset_option('display.max_rows')

# DATA
depression = pd.read_csv('../datasets/depression/depression.csv')
# check the head of depression data
print("Depression data head: ")
print_full(depression.head())


# Step 1:
# Some basic visualization

# instantiate a figure
# 8,8 specify fig height and width in inches
fig = plt.figure(figsize=(12,10))
fig.suptitle('Clinical Depression Measures By Age',
             fontsize=20,
             color='black',
            )


# can add a subplot
# sets up a 2x2 grid and this assings this subplot in the figure
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)
# ax4 = fig.add_subplot(224)

# add some axes
# first value is extreme left of figure, 0 = left, 1 = right
# 2nd where the bottom axis positioned. 0 = bottom, 1 = top
# 3rd width of axis in terms of fraction of entire figure width
# 4th height of axis in terms of fraction of entire figure height
# ax = fig.add_axes([0.0, 0.6, 0.5, 0.4])
# ax.plot()....

# first plot
# data preprocessing
Y_accuteT_placebo = depression[depression['Treat']=="Placebo"]['AcuteT']
X_accuteT_placebo = depression[depression['Treat']=="Placebo"]['Age']

Y_accuteT_Lithium = depression[depression['Treat']=="Lithium"]['AcuteT']
X_accuteT_Lithium = depression[depression['Treat']=="Lithium"]['Age']

Y_accuteT_Imipramine = depression[depression['Treat']=="Imipramine"]['AcuteT']
X_accuteT_Imipramine = depression[depression['Treat']=="Imipramine"]['Age']

# AcuteT versus Age.
ax1.set_title('AcuteT vs Age by Treatment Groups',
              fontsize = 20,
              color = 'black',
              )
ax1.scatter(X_accuteT_placebo, Y_accuteT_placebo,
            color='blue',
            marker='o',
            s=20, #marker size
            label='Placebo',
            )

ax1.scatter(X_accuteT_Lithium, Y_accuteT_Lithium,
            color='red',
            marker='o',
            s=20,
            label='Lithium',
            )

ax1.scatter(X_accuteT_Imipramine, Y_accuteT_Imipramine,
            color='green',
            marker='o',
            s=20,
            label='Imipramine',
            )

# Legends and Labels
ax1.set_label('AcuteT')
ax1.legend()

# Plot Axes and Backdrop
ax1.grid(b=True,
         which='major',
         axis='both',
         alpha=0.5,
        )
ax1.locator_params(axis='x', nbins=10)
ax1.locator_params(axis='y', nbins=10)

# Xlabel, Ylabels
ax1.set_xlabel('Age [years]',
           fontsize=15,
           color='black',
           )
ax1.set_ylabel('AcuteT [days]',
           fontsize=15,
           color='black',
           )

# multiple axis for side-by-side figures
# ax2.scatter =
# ax2.scatter = fig.add_axes([0.0, 0.0, 0.5, 0.4])
# ax2 = ax2.plot()....

# another plot...
# plt.scatter()

# in non-interactive mode, explicitly call the show function
# note: in pycharm this is already in interactive mode, even
# if it says otherwise
print("matplotlib interactive? ", matplotlib.is_interactive())
# matplotlib.interactive(True)


# Step 2:
# make some basic configurations to the backend
# use the interactive backend MacOSX

# Figure title
# some code here...

# # Xlabel, Ylabels
# plt.xlabel('Age [years]',
#            fontsize=15,
#            color='black',
#            )
# plt.ylabel('AcuteT [days]',
#            fontsize=15,
#            color='black',
#            )


# XY ticks
plt.tick_params(axis='y',
                color = 'black',
                labelcolor='black',
                labelsize='large',
                )
plt.tick_params(axis='x',
                color = 'black',
                labelcolor='black',
                # bottom=False,
                # labelbottom=False,
                labelsize='large',
                )
# XY lims
# plt.xlim(1,5)
# plt.ylim(1,5)

# Step 4:
matplotlib.pyplot.grid(b=True,
                       which='major',
                       axis='both',
                       )

# Step 3:
# Adding watermarks
# ax1.text(30, 100,
#         'jaime.meriz13@gmail.com', #water mark text
#         fontsize = 20,
#         color = 'red',
#         ha='left',
#         va='bottom',
#          rotation = 45,
#         alpha = 0.25
#         )

# fig.text # figure level water mark
# can also use the image command for a picture watermark

# Step Final:

# show the legend
# plt.legend()

# show the plot
plt.show()




